# -*- coding: utf-8 -*-
"""
Created on Tue Dec 20 14:52:03 2016

@author: lsteunou
"""
import numpy as np
import random
from pylab import *
import matplotlib.pylab as plt

def fonction_activation1(x):
    return 1.0/(1+np.exp(-x))

def derive_activation1(x):
    return (np.exp(-x)/((1+np.exp(-x))*(1+np.exp(-x))))

class Architecture:
    def __init__(self,archi):
        self.neurones = archi
        self.taille = len(archi)
        self.poids = [np.random.randn(x,y) for x,y in zip(archi[:-1],archi[1:])]
        self.biais = [np.random.randn(1, y) for y in archi[1:]]

    def propagation(self,x):
        for c, b in zip(self.poids, self.biais):
            x = fonction_activation1(np.dot(x,c)+b)
        return x

    def training_prop(self,x):
        prod_scal = []
        signal = np.array([x])
        signaux = [signal]
        for c, b in zip(self.poids, self.biais):
            ps=np.dot(signal,c)+b
            prod_scal.append(ps)
            signal = fonction_activation1(ps)
            signaux.append(signal)
        return (signaux,prod_scal)

    def cout(self, y, sorties_réseau):
        return (y-sorties_réseau)

    def training_retro(self,x,y):
        delta_poids = [np.zeros(c.shape) for c in self.poids]
        delta_biais = [np.zeros(b.shape) for b in self.biais]
        infos = self.training_prop(x)
        sig = infos[0]
        prod_scal = infos[1]

        #Calcul de l'erreur pour la dernière couche
        delta = self.cout(y,sig[-1])*derive_activation1(prod_scal[-1])
        delta_poids[-1] = np.dot(sig[-2].transpose(),delta)
        delta_biais[-1] = delta

        #Calcul de l'erreur pour les couches interne
        for l in range(2,self.taille):
            delta = np.dot(delta,self.poids[-l+1].transpose())*derive_activation1(prod_scal[-l])
            delta_poids[-l] = np.dot(sig[-l-1].transpose(),delta)
            delta_biais[-l] = delta
        return(delta_poids,delta_biais)

    def evaluate(self,testing_data):
        results = [(float(self.propagation(x)),y) for (x,y) in testing_data]
        erQuad = sum(((y-x)*(y-x))/2 for (x,y) in results)
        return(erQuad)

    def training(self,training_data,epochs,taux,batch_size,testing_data=None):
        err = []
        for j in range(epochs):
            np.random.shuffle(training_data)
            batches = [training_data[ind:ind+batch_size] for ind in range(0,len(testing_data),batch_size)]
            for batch in batches:
                mean_loss_poids = [np.zeros(c.shape) for c in self.poids]
                mean_loss_biais = [np.zeros(b.shape) for b in self.biais]
                for x,y in batch:
                    delta_poids, delta_biais = self.training_retro(x,y)
                    mean_loss_biais = [mlb+db for mlb, db in zip(mean_loss_biais, delta_biais)]
                    mean_loss_poids = [mlp+dp for mlp, dp in zip(mean_loss_poids, delta_poids)]
                self.poids = [c+(taux/len(batch))*mlp for c, mlp in zip(self.poids, mean_loss_poids)]
                self.biais = [b+(taux/len(batch))*mlb for b, mlb in zip(self.biais, mean_loss_biais)]
            if testing_data:
                print("Etape {0} : Erreur quadratique = {1}".format(j,self.evaluate(testing_data)))
            else:
                print("Etape {0} terminer".format(j))
            err.append(self.evaluate(testing_data))
        return(err)

archi = Architecture([1,6,1])

#Construction de la base d'apprentissage
xa = np.linspace(0,2*np.pi,10000)
ya = 0.5*(np.sin(xa)+1)
base_ap = [(x,y) for x,y in zip(xa,ya)]

#Construction de la base test
xt = np.linspace(0,2*np.pi,2000)
yt = 0.5*(np.sin(xt)+1)
base_test = [(x,y) for x,y in zip(xt,yt)]

err = archi.training(base_ap,5000,1.5,100,base_test)

sortie = []
for x in xt:
    sortie.append(archi.propagation(x)[0][0])

plt.plot(xt,yt)
plt.plot(xt,sortie)
plt.show()
