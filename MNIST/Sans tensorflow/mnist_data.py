#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 28 19:50:29 2016

@author: lsteunou
"""
import _pickle 
import numpy as np
import gzip

def load_data():
    f=gzip.open('mnist.pkl.gz','rb')
    training_data, validation_data, test_data = _pickle.load(f,encoding='latin1')
    f.close()
    return(training_data, validation_data, test_data)

def vectorized_result(j):
    e = np.zeros((10, 1))
    e[j] = 1.0
    return e
    
def data():
    tr_d, va_d, te_d = load_data()
    training_inputs = [np.reshape(x, (784, 1)) for x in tr_d[0]]
    training_results = [vectorized_result(y) for y in tr_d[1]]
    training_data = [(x,y) for x,y in zip(training_inputs,training_results)]
    validation_inputs = [np.reshape(x, (784, 1)) for x in va_d[0]]
    validation_data = [(x,y) for x,y in zip(validation_inputs,va_d[1])]
    test_inputs = [np.reshape(x, (784, 1)) for x in te_d[0]]
    test_data = [(x,y) for x,y in zip(test_inputs,te_d[1])]
    return (training_data, validation_data, test_data)
    
t,v,te=data()

