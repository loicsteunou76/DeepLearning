# -*- coding: utf-8 -*-
"""
Created on Tue Dec 20 16:08:11 2016

@author: lsteunou
"""

import numpy as np
import random
from pylab import *

def fonction_activation1(x):
    return 1.0/(1+np.exp(-x))
    
def derive_activation1(x):
    return (np.exp(-x)/((1+np.exp(-x))*(1+np.exp(-x))))
    
class Architecture:
    def __init__(self,archi):
        self.neurones = archi
        self.taille = len(archi)
        self.poids = [np.random.randn(x,y) for x,y in zip(archi[:-1],archi[1:])]
        self.biais = [np.random.randn(1, y) for y in archi[1:]]
    
    def propagation(self,x):
        signal = np.transpose(np.array(x))
        for c, b in zip(self.poids, self.biais):
            signal = fonction_activation1(np.dot(signal,c)+b)
        return signal
                      
    def training_prop(self,x):
        prod_scal = []
        signal = np.transpose(np.array(x))
        signaux = [signal]
        for c, b in zip(self.poids, self.biais):
            ps=np.dot(signal,c)+b
            prod_scal.append(ps)
            signal = fonction_activation1(ps)
            signaux.append(signal)
        return (signaux,prod_scal)
        
    def cout(self,y,sorties_réseau):
        return (y-sorties_réseau)
            
    def training_retro(self,x,y):
        delta_poids = [np.zeros(c.shape) for c in self.poids]
        delta_biais = [np.zeros(b.shape) for b in self.biais]
        infos = self.training_prop(x)
        sig = infos[0]
        prod_scal = infos[1]
        y = np.transpose(np.array(y))
        #Calcul de l'erreur pour la dernière couche
        delta = self.cout(y,sig[-1])*derive_activation1(prod_scal[-1])
        delta_poids[-1] = np.dot(sig[-2].transpose(),delta)
        delta_biais[-1] = delta
        #Calcul de l'erreur pour les couches interne
        for l in range(2,self.taille):
            delta = np.dot(delta,self.poids[-l+1].transpose())*derive_activation1(prod_scal[-l])
            delta_poids[-l] = np.dot(sig[-l-1].transpose(),delta)
            delta_biais[-l] = delta
        return(delta_poids,delta_biais)
        
    def evaluate(self,test_data):
        results = [(np.argmax(self.propagation(x)), y) for (x, y) in test_data]       
        return sum(int(x == y) for (x, y) in results)
        
    def training(self,training_data,epochs,taux,batch_size,testing_data=None):
        acc = []
        for j in range(epochs):
            np.random.shuffle(training_data)
            batches = [training_data[ind:ind+batch_size] for ind in range(0,len(testing_data),batch_size)]
            for batch in batches:
                mean_loss_poids = [np.zeros(c.shape) for c in self.poids]
                mean_loss_biais = [np.zeros(b.shape) for b in self.biais]
                for x,y in batch:
                    delta_poids, delta_biais = self.training_retro(x,y)
                    mean_loss_biais = [mlb+db for mlb, db in zip(mean_loss_biais, delta_biais)]
                    mean_loss_poids = [mlp+dp for mlp, dp in zip(mean_loss_poids, delta_poids)]
                self.poids = [c+(taux/len(batch))*mlp for c, mlp in zip(self.poids, mean_loss_poids)]
                self.biais = [b+(taux/len(batch))*mlb for b, mlb in zip(self.biais, mean_loss_biais)]
            if testing_data:
                print("Etape {0}: {1}/{2}".format(j,self.evaluate(testing_data),len(testing_data)))
            else:
                print("Etape {0} terminer".format(j))
            acc.append(self.evaluate(testing_data))
        return(acc)
                


