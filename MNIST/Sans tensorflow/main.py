#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 29 15:23:44 2016

@author: lsteunou
"""

import mnist_data
import mnistNN
import gzip

training_data, validation_data, test_data = mnist_data.data()
neural_network = mnistNN.Architecture([784,30,10])

acc = neural_network.training(training_data,100,3.0,25,test_data)
